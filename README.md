![GitHub Logo](ffmpeg-summer-v2.png)

### 1. Getting audio/video file information 

	ffmpeg -i video.mp4 -hide_banner 

>	`use '-hide_banner' to see only metadata`

### 2. Converting video files to different formats

	ffmpeg -i video.mp4 video.avi
	ffmpeg -i video.flv video.mpeg

>	`If you want to preserve the quality of your source video file, use ‘-qscale 0’ parameter:`

	ffmpeg -i input.webm -qscale 0 output.mp4

>	`To check list of supported formats by FFmpeg, run:`
	
	ffmpeg -formats

### 3. Converting video files to audio files

>	`To convert a video file to audio file, just specify the output format as .mp3, or .ogg, or any other audio formats.`

	ffmpeg -i input.mp4 -vn output.mp3

>	`-vn – Indicates that we have disabled video recording in the output file.`

### 4. Change resolution of video files

	ffmpeg -i input.mp4 -s 1280x720 -c:a copy output.mp4

### 5. Compressing video files

	ffmpeg -i input.mp4 -vf scale=1280:-1 -c:v libx264 -preset veryslow -crf 24 output.mp4

### 6. Compressing Audio files

	ffmpeg -i input.mp3 -ab 128 output.mp3

`The list of various available audio bitrates are:`

- 96kbps
- 112kbps
- 128kbps
- 160kbps
- 192kbps
- 256kbps
- 320kbps

### 7. Removing audio stream from a video file

	ffmpeg -i input.mp4 -an output.mp4

>	`Here, ‘an’ indicates no audio recording.`

### 8. Removing video stream from a media file

	ffmpeg -i input.mp4 -vn output.mp3

### 9. Extracting images from the video 

	ffmpeg -i input.mp4 -r 1 -f image2 image-%2d.png

`-r – Set the frame rate. I.e the number of frames to be extracted into images per second. The default value is 25.`

`-f – Indicates the output format i.e image format in our case.`

`image-%2d.png – Indicates how we want to name the extracted images. In this case, the names should start like image-01.png, image-02.png, image-03.png and so on. If you use %3d, then the name of images will start like image-001.png, image-002.png and so on.`

### 10. Cropping videos

	ffmpeg -i input.mp4 -filter:v "crop=w:h:x:y" output.mp4

`input.mp4 – source video file.`

`-filter:v – Indicates the video filter.`

`crop – Indicates crop filter.`

`w – Width of the rectangle that we want to crop from the source video.`

`h – Height of the rectangle.`

`x – x coordinate of the rectangle that we want to crop from the source video.`

`y – y coordinate of the rectangle.`

Let us say you want to a video with a width of 640 pixels and a height of 480 pixels, from the position (200,150), the command would be:

	ffmpeg -i input.mp4 -filter:v "crop=640:480:200:150" output.mp4

### 11. Convert a specific portion of a video
	
	ffmpeg -i input.mp4 -t 10 output.avi

### 12. Set the aspect ratio to video

	ffmpeg -i input.mp4 -aspect 16:9 output.mp4

`The commonly used aspect ratios are:`
- 16:9
- 4:3
- 16:10
- 5:4
- 2:21:1
- 2:35:1
- 2:39:1

### 13. Adding poster image to audio files

	ffmpeg -loop 1 -i inputimage.jpg -i inputaudio.mp3 -c:v libx264 -c:a aac -strict experimental -b:a 192k -shortest output.mp4

### 14. Trim a media file using start and stop times

	ffmpeg -i input.mp4 -ss 00:00:50 -codec copy -t 50 output.mp4

`–s – Indicates the starting time of the video clip. In our example, starting time is the 50th second.`

`-t – Indicates the total time duration.`

	ffmpeg -i audio.mp3 -ss 00:01:54 -to 00:06:53 -c copy output.mp3

### 15. Split video files into multiple parts

	ffmpeg -i input.mp4 -t 00:00:30 -c copy part1.mp4 -ss 00:00:30 -codec copy part2.mp4

### Map Video and Audio
	ffmpeg -i test.mp4 -i audio.mp3 -c:v copy -map 0:v:0 -map 1:a:0 new.mp4

### Resize Images ( Scale )
	
	ffmpeg -i vert7.png -vf scale=480:620 vert7_480x620.png
	
### Create a video from image

	ffmpeg -r 1/5 -i krishna.jpg -c:v libx264 -vf fps=25 -pix_fmt yuv420p out.mp4

	

